import { v4 as uuidv4 } from "uuid";
import { CurrencyEnum } from "./modules/App.ts";
export class Transaction {
  Id: string;
  Amount: number;
  Currency: CurrencyEnum;
  constructor(Amount: number, Currency: CurrencyEnum) {
    this.Id = uuidv4();
    this.Amount = Amount;
    this.Currency = Currency;
  }
}

export class Card {
  TransactionInput: Transaction;
  Transactions: Transaction[];
  constructor() {
    this.Transactions = [];
  }

  AddTransaction = (
    TransactionOrAmount?: Transaction | number,
    Currency?: CurrencyEnum
  ): string => {
    const createTransaction = (Amount: number, Currency: CurrencyEnum) => {
      const newTransaction = new Transaction(Amount, Currency);
      this.Transactions.push(newTransaction);
      return newTransaction.Id;
    };
    if (typeof TransactionOrAmount !== "number" && !Currency) {
      return createTransaction(
        TransactionOrAmount.Amount,
        TransactionOrAmount.Currency
      );
    } else if (
      typeof TransactionOrAmount === "number" &&
      TransactionOrAmount &&
      Currency
    ) {
      return createTransaction(TransactionOrAmount, Currency);
    } else throw new Error("Required transaction field is empty!");
  };

  GetTransaction = (id: string) => {
    return this.Transactions.find((transaction) => (transaction.Id = id));
  };

  GetBalance = (currency: CurrencyEnum) => {
    const transactionsByCurrency = this.Transactions.filter(
      (transaction) => transaction.Currency == currency
    ).map((transaction) => transaction.Amount);
    const totalAmount = transactionsByCurrency.length
      ? transactionsByCurrency?.reduce((prev, current) => prev + current)
      : null;
    return totalAmount ?? "Not found any money for this currency...";
  };
}
