Task
Create a class system for working with money and cards.<br>

Technical Requirements:<br>
Create an enum called CurrencyEnum with values USD and UAH.<br>
Create a class called Transaction that has Id (a unique transaction identifier), Amount (the amount of money), and Currency (the currency, using the mentioned enum). The Id should be generated when a new instance of the class is created (using uuid). Other parameters are passed to the constructor.<br>
Create a class called Card. It should contain a list of transactions and the following methods:<br>
The AddTransaction method that takes a Transaction as input. It should add the transaction to the list of transactions inside the card and return the Id of the transaction.<br>
Overload the AddTransaction method to accept Currency and Amount as input. It should create a new transaction, add it to the list of transactions inside the card, and return the Id of the transaction.<br>
The GetTransaction method that takes an Id as input and returns the transaction from the card's list with the specified Id.<br>
The GetBalance method that takes Currency as input and returns the total amount of money for all transactions on the card in the specified currency.<br>
All the above conditions should be implemented using TypeScript and type capabilities to the maximum extent possible.
Create a test.ts file to demonstrate the usage of the Card entity.