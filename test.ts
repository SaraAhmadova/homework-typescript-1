import { Card, Transaction } from ".";
import { CurrencyEnum } from "./modules/App";

const NewCard = new Card();

const Transaction1 = new Transaction(100, CurrencyEnum.USD);
NewCard.AddTransaction(500, CurrencyEnum.USD);
const transactionId = NewCard.AddTransaction(Transaction1);
console.log(NewCard.Transactions);
console.log(NewCard.GetTransaction(transactionId));
console.log(NewCard.GetBalance(CurrencyEnum.USD));
console.log(NewCard.GetBalance(CurrencyEnum.UAH));